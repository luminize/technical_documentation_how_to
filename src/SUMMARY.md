# Summary

- [Introduction](./Introduction.md)
  - [Example diagram code](./code_example.md)
  - [Advantages](./advantages.md),
  - [This guide...](./this_guide.md)
- [Tools](./Tools.md)
  - [Git](./tools/Git.md)
  - [Docker](./tools/Docker.md)
  - [PlantUML server](./tools/PlantUMLserver.md)
  - [Visual Studio Code](./tools/VSCode.md)
  - [mdBook](./tools/mdBook.md)
- [Usage](./Usage.md)
  - [Git clone](./usage/Git.md)
  - [PlantUML render example](./usage/PlantUML.md)
  - [mdBook](./usage/mdBook.md)
